ansible-vagrant
===============

必要なもの
----------

- VirtualBox
- Vagrant
- Ansible

使い方
------

```
$ git clone https://gitlab.com/matthew70/ansible-vagrant.git
$ cd ansible-vagrant
$ vagrant up
...
$ vagrant ssh
```

オプション
----------

`ANSIBLE_TAGS` 環境変数にプラグイン名を入れることで希望の環境を作れます。

### Gitフレンドリーな`bash`プロンプトを使いたい時

```
$ ANSIBLE_TAGS=bash-prompt vagrant up
```

### Python環境を整えたいとき

```
$ ANSIBLE_TAGS=python vagrant up
```

### PROMPT + Python

```
$ ANSIBLE_TAGS="bash-prompt,python" vagrant up
```

### 使えるプラグイン

- [bash-prompt](provisioning/bash-prompt.yml)
- [python](provisioning/python.yml)

### 実装したいプラグイン

- mysql
- postgresql
- redis
- python-django
- python-flask
- sass
